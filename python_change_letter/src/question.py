ERROR_NOT_FOUND = '-1'
ERROR_INVALID = '-2'

#Given a string, find all occurrences of badLtr and replace them with goodLtr. If you cannot find any
#of badLtr, return ERROR_NOT_FOUND. If an invalid string is passed in, return ERROR_INVALID
def ChangeLetter(inputStr, badLtr, goodLtr):
    if len(inputStr) < 1: return ERROR_INVALID
    try:
        inputStr.index(badLtr)
    except: return ERROR_NOT_FOUND
    outputStr = inputStr.replace(badLtr, goodLtr)
    return outputStr