import unittest
from python_change_letter.src.question import ChangeLetter

ERROR_NOT_FOUND = '-1'
ERROR_INVALID = '-2'

chngLetters = {'bad1': 'a', 'str1': 'thas as the incorrect strang', 'good1': 'i', \
               'bad2': 'q', 'str2': 'wily werewolf leaves scary experimentation for next year', 'good2': 'r'}

correctSentences = ['this is the incorrect string', 'A single string is used for the address family',\
                    'Fo~*~bo*ybe}*homcd+', 'The conversion field causes a type coercion before formatting', \
                    'This function does the actual work of formatting', 'this is the  string']

class TestChangeLetter(unittest.TestCase):
    def test_ChangeLetter(self):
        output = ChangeLetter(chngLetters['str1'], chngLetters['bad1'], chngLetters['good1'])
        self.assertEqual(output, correctSentences[0])
    def test_ChangeLetterBad(self):
        output = ChangeLetter(chngLetters['str2'], chngLetters['bad2'], chngLetters['good2'])
        self.assertEqual(output, ERROR_NOT_FOUND)
    def test_ChangeLetterNone(self):
        output = ChangeLetter('', 'a', 'x')
        self.assertEqual(output, ERROR_INVALID)