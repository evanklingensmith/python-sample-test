import unittest
from python_reverse_word.src.question import ReverseWord

ERROR_NOT_FOUND = '-1'
ERROR_INVALID = '-2'

reverseWords = {'bad1': 'elgnis', 'str1': 'A elgnis string is used for the address family', 'bad2': 'srorre', \
                'str2': 'All errors esiar exceptions'}

correctSentences = ['A single string is used for the address family']

class TestReverseWord(unittest.TestCase):
    def test_ReverseWord(self):
        output = ReverseWord(reverseWords['str1'], reverseWords['bad1'])
        self.assertEqual(output, correctSentences[0])
    def test_ReverseWordBad(self):
        output = ReverseWord(reverseWords['str2'], reverseWords['bad2'])
        self.assertEqual(output, ERROR_NOT_FOUND)
    def test_ReverseWordNone(self):
        output = ReverseWord('', reverseWords['bad2'])
        self.assertEqual(output, ERROR_INVALID)