#Given a string and a word, find the word in the sentence and reverse it. Return the corrected string if the word is
#found, or ERROR_NOT_FOUND if the word is not in the string, or ERROR_INVALID if a parameter is invalid
ERROR_NOT_FOUND = '-1'
ERROR_INVALID = '-2'

def ReverseWord(inputStr, revWord):
    if len(inputStr) < 1: return ERROR_INVALID
    if len(revWord) < 1: return ERROR_INVALID
    try:
        inputStr.index(revWord)
    except: return ERROR_NOT_FOUND
    outputStr = inputStr.replace(revWord, revWord[::-1])
    return outputStr