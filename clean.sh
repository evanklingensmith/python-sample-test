#/bin/sh
find . -type d -name __pycache__ | xargs rm -rf
find . -type f -name '*.py[co]' -delete
