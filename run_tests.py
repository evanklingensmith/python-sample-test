import unittest, xmlrunner, os
import requests
from conf import SERVER, STUDENT_ID, EVENT_ID, KEY

_url = 'http://{}/{}/{}/addunittestlog/'.format(SERVER, EVENT_ID, STUDENT_ID)

if __name__ == '__main__':
    suite = unittest.TestLoader().discover(start_dir='.', pattern='test_case.py')
    results = xmlrunner.XMLTestRunner(verbosity=2, output='../test-reports').run(suite)

    for dirname, dirnames, filenames in os.walk('../test-reports'):
        for filename in filenames:
            with open(os.path.join(dirname, filename), 'r') as send_me:
                _data = send_me.read().replace('\n', '')
                requests.post(url=_url, data=_data, headers={'key':KEY})
            os.remove(os.path.join(dirname, filename))

