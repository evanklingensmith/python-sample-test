import os

#Given a directory, traverse through all sub-directories and build a dictionary with the directory as the key and a list
#of the .txt files as the value
def FileDict(directory):
    fileDict = {}
    for root, dirs, files in os.walk(directory):
        textfiles = []
        for name in files:
            if(os.path.splitext(name)[1] == '.txt'):
                textfiles.append(name)
            if(len(textfiles) > 0):
                # correct
                # fileDict[root.replace('/', '\\')] = textfiles
                fileDict[root] = textfiles
    return fileDict