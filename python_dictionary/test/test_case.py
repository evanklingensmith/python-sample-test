import unittest
import os
from python_dictionary.src.question import FileDict

testDir = 'testFolder'

correctDict = {'testFolder\subfolder1\subfolder2': ['text_fil4_1.txt', 'txt98.txt'], 'testFolder\subfolder1': \
    ['23file.txt', '56fil.txt'], 'testFolder': ['file1.txt', 'file2.txt']}

class TestDictionaryMethod(unittest.TestCase):
    def test_dictionary(self):
        prev_dir = os.getcwd()
        os.chdir(os.getcwd() + '/python_dictionary/test/')
        output = FileDict(testDir)
        for key in output:
            output[key].sort()
        os.chdir(prev_dir)
        self.assertEqual(output, correctDict)